Responsive Menu Sample
==================

ウィンドウサイズにより、Menuの表示の仕方をcssで切り替えるサンプル。  

詳細な説明は、以下のブログエントリを参照ください。
[レスポンシブなサイドメニューを表示させる](http://mid0111.hatenablog.com/entry/2014/07/11/232846)

## Usage

```bash
$ npm install
$ gulp serve
```

