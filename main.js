
(function() {

  'use strict';

  var querySelector = document.querySelector.bind(document);
  var menuPanel = querySelector('.menu-panel');
  var hambuger = querySelector('.hamburger');
  var swipe = querySelector('.swipe');
  var main = querySelector('.main');
  var fadeLayer = querySelector('.fade-layer');

  function toggleMenu() {
    var currentVisibility = fadeLayer.style.visibility;
    var currentColr = fadeLayer.style.backgroundColor;
    if (currentVisibility === 'visible') {
		  menuPanel.classList.remove('menu-open' );
      fadeLayer.style.backgroundColor = 'rgba(0, 0, 0, 0)';
      fadeLayer.style.visibility = 'hidden';
    } else {
		  menuPanel.classList.add('menu-open' );
      fadeLayer.style.visibility = 'visible';
      fadeLayer.style.backgroundColor = '#000000';
    }

  };

  // Menu open
  hambuger.addEventListener('click', toggleMenu);
  swipe.addEventListener('touchmove', function(event) {
    toggleMenu();
  });

  // Menu close
  fadeLayer.addEventListener('click', toggleMenu);
  fadeLayer.addEventListener('touchmove', function(event) {
    toggleMenu();
  });

})();
